import numpy as np

# t1 = np.arange(12).reshape(4,3)
# t2 = np.arange(0,20).reshape(5,4)
# t3 = np.tile(t1,[5,1,1])
# print t1.shape
# print t2.shape
# print t3.shape
#
# print t3 + t1  # la prima operazione funziona perche crea dei jolly a sx di t1
# #  print t3 + t2  # NO!! la seconda no perche dovrebbe creare dei jolly a dx di t2
#
# # prova coi vector
# v1 = np.array([2, 3, 1])
# v2 = np.array([7, 13, 1, 9])
# print v1.shape
# print v2.shape
#
# print t1+v1
# # print t1+v2 # NO!! l'ultima dimensione di t1 deve essere uguale a quella non broadcastable di v
#
# # prova con dot product
# d1 = np.random.randn(4, 3)
# d2 = np.random.randn(3, 7)
# d3 = np.random.randn(8, 9, 10)
# d4 = np.random.randn(10, 9, 7)
#
#
# # print np.dot(d1,d2)
# # print np.dot(d1,v1)
# # print np.dot(d1,v2)  # NO! le dimensioni interne non coincidono
# print np.dot(d3,d4)

t4 = np.random.rand(3,4,5,6)
t5 = np.random.rand(6,7)
print np.dot(t4,t5).shape

t4 = np.random.rand(3,4,5,6)
t5 = np.random.rand(8,6,7)
print np.dot(t4,t5).shape

t4 = np.random.rand(3,4,5,6)
t5 = np.random.rand(9,3,6,7)
print np.dot(t4,t5).shape

d3 = np.random.randn(8, 9, 10)
d4 = np.random.randn(4, 10, 3)
print np.dot(d3,d4).shape

