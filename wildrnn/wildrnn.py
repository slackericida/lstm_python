# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from wild_utils import *
from hf import hf_optimizer
from datetime import datetime
# theano.config.exception_verbosity = "high"
theano.config.optimizer = "fast_compile"  # usefull for debug


class RNNTheano:

    def __init__(self, input_dim, output_dim, hidden_dim, bptt_truncate=4):
        # Assign instance variables
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.bptt_truncate = bptt_truncate

        # Randomly initialize the network parameters
        Win = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (hidden_dim, input_dim))
        Wout = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (output_dim, hidden_dim))
        W = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        bh = np.zeros(hidden_dim)
        by = np.zeros(output_dim)

        # ----- SHARED VARIABLES -----
        # Network parameters
        self.Win = theano.shared(name='Win', value=Win.astype(theano.config.floatX))
        self.Wout = theano.shared(name='Wout', value=Wout.astype(theano.config.floatX))
        self.W = theano.shared(name='W', value=W.astype(theano.config.floatX))
        self.bh = theano.shared(name='bh', value=bh.astype(theano.config.floatX))
        self.by = theano.shared(name='by', value=by.astype(theano.config.floatX))

        # Quantities proportional to network parameters (used in gradient descent)
        self.Win_1 = theano.shared(name='Win_1', value=np.zeros(Win.shape).astype(theano.config.floatX))
        self.Wout_1 = theano.shared(name='Wout_1', value=np.zeros(Wout.shape).astype(theano.config.floatX))
        self.W_1 = theano.shared(name='W_1', value=np.zeros(W.shape).astype(theano.config.floatX))
        self.bh_1 = theano.shared(name='bh_1', value=np.zeros(bh.shape).astype(theano.config.floatX))
        self.by_1 = theano.shared(name='by_1', value=np.zeros(by.shape).astype(theano.config.floatX))
        self.Win_2 = theano.shared(name='Win_2', value=np.zeros(Win.shape).astype(theano.config.floatX))
        self.Wout_2 = theano.shared(name='Wout_2', value=np.zeros(Wout.shape).astype(theano.config.floatX))
        self.W_2 = theano.shared(name='W_2', value=np.zeros(W.shape).astype(theano.config.floatX))
        self.bh_2 = theano.shared(name='bh_2', value=np.zeros(bh.shape).astype(theano.config.floatX))
        self.by_2 = theano.shared(name='by_2', value=np.zeros(by.shape).astype(theano.config.floatX))

        # gradient update hyperparameters
        self.mu = theano.shared(name='mu', value=0.9)
        self.mu2 = theano.shared(name='mu2', value=0.9)

        # learning rate
        self.learning_rate = theano.shared(name='learning_rate', value=0.01)

        # current iteration
        self.iteration = theano.shared(name='curr_iteration', value=1)

        # L1 and L2 regularization parameters
        self.reg_lambda1 = theano.shared(name='lambda1', value=0.0)
        self.reg_lambda2 = theano.shared(name='lambda2', value=0.0)

        # dropout probability
        self.p_drop = theano.shared(name='p_drop', value=1.0)

        # We store the Theano graph here
        # self.theano = {}
        self.__theano_build__()

    def __theano_build__(self):

        # Initialize variables
        x = T.matrix('input vec')
        y = T.matrix('target_vec')
        srng = RandomStreams(seed=12345)

        # calculate output with forward propagation
        def forward_prop_step(x_t, s_t_prev, Win, Wout, W, bh, by, p_drop):
            s_t = T.tanh(T.dot(Win, x_t) + T.dot(W, s_t_prev) + bh)
            if T.lt(p_drop, 1):  # If the dropout probability is < 1, set neuron's output to zero with probability p
                s_t * srng.binomial(size=(self.hidden_dim,), p=p_drop) / p_drop
            o_t = T.dot(Wout, s_t) + by
            return o_t, s_t

        [o, _], _ = theano.scan(
            forward_prop_step,
            sequences=x,
            outputs_info=[None, np.zeros(self.hidden_dim)],
            non_sequences=[self.Win, self.Wout, self.W, self.bh, self.by, self.p_drop],
            truncate_gradient=self.bptt_truncate,
            strict=True)

        # Loss function
        o_error = nrmse(o, y) + regularize([self.Win, self.Wout, self.W], reg_lambda1=self.reg_lambda1, reg_lambda2=self.reg_lambda2)

        # Gradients
        dWin = T.grad(o_error, self.Win)
        dWout = T.grad(o_error, self.Wout)
        dW = T.grad(o_error, self.W)
        dbh = T.grad(o_error, self.bh)
        dby = T.grad(o_error, self.by)

        # Theano functions
        self.forward_propagation = theano.function(inputs=[x], outputs=o)
        self.calculate_loss = theano.function(inputs=[x, y], outputs=o_error)
        self.bptt = theano.function(inputs=[x, y], outputs=[dWin, dWout, dW, dbh, dby])

        # Vanilla gradient update
        self.sgd_step_vanilla = theano.function(inputs=[x, y],
                                                outputs=[],
                                                updates=[
                                                     (self.Win, self.Win - self.learning_rate * dWin),
                                                     (self.Wout, self.Wout - self.learning_rate * dWout),
                                                     (self.W, self.W - self.learning_rate * dW),
                                                     (self.bh, self.bh - self.learning_rate * dbh),
                                                     (self.by, self.by - self.learning_rate * dby)])

        # Momentum gradient update
        self.sgd_step_momentum = theano.function(inputs=[x, y],
                                                 outputs=[],
                                                 updates=[
                                                     # integrate velocity
                                                     (self.Win_1, self.mu * self.Win_1 - self.learning_rate * dWin),
                                                     (self.W_1, self.mu * self.W_1 - self.learning_rate * dW),
                                                     (self.Wout_1, self.mu * self.Wout_1 - self.learning_rate * dWout),
                                                     (self.bh_1, self.mu * self.bh_1 - self.learning_rate * dbh),
                                                     (self.by_1, self.mu * self.by_1 - self.learning_rate * dby),
                                                     # integrate position
                                                     (self.Win, self.Win + self.Win_1),
                                                     (self.Wout, self.Wout + self.Wout_1),
                                                     (self.W, self.W + self.W_1),
                                                     (self.bh, self.bh + self.bh_1),
                                                     (self.by, self.by + self.by_1)])

        # Nesterov gradient update
        self.sgd_step_nesterov = theano.function(inputs=[x, y],
                                                 outputs=[],
                                                 updates=[
                                                     # back this up
                                                     (self.Win_2, self.Win_1),
                                                     (self.W_2, self.W_1),
                                                     (self.Wout_2, self.Wout_1),
                                                     (self.bh_2, self.bh_1),
                                                     (self.by_2, self.by_1),
                                                     # integrate velocity
                                                     (self.Win_1, self.mu * self.Win_1 - self.learning_rate * dWin),
                                                     (self.W_1, self.mu * self.W_1 - self.learning_rate * dW),
                                                     (self.Wout_1, self.mu * self.Wout_1 - self.learning_rate * dWout),
                                                     (self.bh_1, self.mu * self.bh_1 - self.learning_rate * dbh),
                                                     (self.by_1, self.mu * self.by_1 - self.learning_rate * dby),
                                                     # integrate position
                                                     (self.Win, self.Win - self.mu * self.Win_2 + (1 + self.mu) * self.Win_1),
                                                     (self.Wout, self.Wout - self.mu * self.Wout_2 + (1 + self.mu) * self.Wout_1),
                                                     (self.W, self.W - self.mu * self.W_2 + (1 + self.mu) * self.W_1),
                                                     (self.bh, self.bh - self.mu * self.bh_2 + (1 + self.mu) * self.bh_1),
                                                     (self.by, self.by - self.mu * self.by_2 + (1 + self.mu) * self.by_1)])

        # Adagrad gradient update
        self.sgd_step_adagrad = theano.function(inputs=[x, y],
                                                outputs=[],
                                                updates=[
                                                     # update local strength
                                                     (self.Win_1, self.Win_1 + dWin**2),
                                                     (self.W_1, self.W_1 + dW**2),
                                                     (self.Wout_1, self.Wout_1 + dWout**2),
                                                     (self.bh_1, self.bh_1 + dbh**2),
                                                     (self.by_1, self.by_1 + dby**2),
                                                     # update variable with local strength
                                                     (self.Win, self.Win - self.learning_rate * dWin / T.sqrt(self.Win_1 + 1e-8)),
                                                     (self.Wout, self.Wout - self.learning_rate * dWout / T.sqrt(self.Wout_1 + 1e-8)),
                                                     (self.W, self.W - self.learning_rate * dW / T.sqrt(self.W_1 + 1e-8)),
                                                     (self.bh, self.bh - self.learning_rate * dbh / T.sqrt(self.bh_1 + 1e-8)),
                                                     (self.by, self.by - self.learning_rate * dby / T.sqrt(self.by_1 + 1e-8))])

        # RMSprop gradient update (damped Adagrad)
        self.sgd_step_rmsprop = theano.function(inputs=[x, y],
                                                outputs=[],
                                                updates=[
                                                     # update local strength
                                                     (self.Win_1, self.mu * self.Win_1 + (1 - self.mu) * dWin**2),
                                                     (self.W_1, self.mu * self.W_1 + (1 - self.mu) * dW**2),
                                                     (self.Wout_1, self.mu * self.Wout_1 + (1 - self.mu) * dWout**2),
                                                     (self.bh_1, self.mu * self.bh_1 + (1 - self.mu) * dbh**2),
                                                     (self.by_1, self.mu * self.by_1 + (1 - self.mu) * dby**2),
                                                     # update variable with local strength
                                                     (self.Win, self.Win - self.learning_rate * dWin / T.sqrt(self.Win_1 + 1e-8)),
                                                     (self.Wout, self.Wout - self.learning_rate * dWout / T.sqrt(self.Wout_1 + 1e-8)),
                                                     (self.W, self.W - self.learning_rate * dW / T.sqrt(self.W_1 + 1e-8)),
                                                     (self.bh, self.bh - self.learning_rate * dbh / T.sqrt(self.bh_1 + 1e-8)),
                                                     (self.by, self.by - self.learning_rate * dby / T.sqrt(self.by_1 + 1e-8))])

        # Adam gradient update
        self.sgd_step_adam = theano.function(inputs=[x, y],
                                             outputs=[],
                                             updates=[
                                                 # update first momentum of past gradients
                                                 (self.Win_1, self.mu * self.Win_1 + (1 - self.mu) * dWin),
                                                 (self.W_1, self.mu * self.W_1 + (1 - self.mu) * dW),
                                                 (self.Wout_1, self.mu * self.Wout_1 + (1 - self.mu) * dWout),
                                                 (self.bh_1, self.mu * self.bh_1 + (1 - self.mu) * dbh),
                                                 (self.by_1, self.mu * self.by_1 + (1 - self.mu) * dby),
                                                 # update second momentum of past gradients
                                                 (self.Win_2, self.mu2 * self.Win_2 + (1 - self.mu2) * dWin**2),
                                                 (self.W_2, self.mu2 * self.W_2 + (1 - self.mu2) * dW**2),
                                                 (self.Wout_2, self.mu2 * self.Wout_2 + (1 - self.mu2) * dWout**2),
                                                 (self.bh_2, self.mu2 * self.bh_2 + (1 - self.mu2) * dbh**2),
                                                 (self.by_2, self.mu2 * self.by_2 + (1 - self.mu2) * dby**2),
                                                 # compute bias-corrected first and second moment estimates
                                                 (self.iteration, self.iteration + 1),
                                                 # update network parameters
                                                 (self.Win, self.Win - self.learning_rate * (T.sqrt(1 - self.mu2**self.iteration) / (1 - self.mu**self.iteration)) * (self.Win_1 / T.sqrt(self.Win_2 + 1e-8))),
                                                 (self.W, self.W - self.learning_rate * (T.sqrt(1 - self.mu2**self.iteration) / (1 - self.mu**self.iteration)) * (self.W_1 / T.sqrt(self.W_2 + 1e-8))),
                                                 (self.Wout, self.Wout - self.learning_rate * (T.sqrt(1 - self.mu2**self.iteration) / (1 - self.mu**self.iteration)) * (self.Wout_1 / T.sqrt(self.Wout_2 + 1e-8))),
                                                 (self.bh, self.bh - self.learning_rate * (T.sqrt(1 - self.mu2**self.iteration) / (1 - self.mu**self.iteration)) * (self.bh_1 / T.sqrt(self.bh_2 + 1e-8))),
                                                 (self.by, self.by - self.learning_rate * (T.sqrt(1 - self.mu2**self.iteration) / (1 - self.mu**self.iteration)) * (self.by_1 / T.sqrt(self.by_2 + 1e-8)))])

        # Hessian Free optimization
        # Arguments: model_parameters, inputs, output_hidden_layer, loss_function
        self.hf_model = hf_optimizer([self.Win, self.W, self.Wout, self.bh, self.by], [x, y], o, [o_error])

    def set_hyperpar(self, mu=0.9, mu2=0.99, learning_rate=0.01, reg_lambda1=0, reg_lambda2=0.0001, p_drop=1):
        self.mu, self.mu2, self.learning_rate, self.reg_lambda2, self.reg_lambda2, self.p_drop = mu, mu2, learning_rate, reg_lambda1, reg_lambda2, p_drop

    # Train model parameters using Hessian Free optimization
    def hf_train(self,
                 training_data,
                 cg_data,
                 valid_data=None,
                 initial_lambda=0.5,
                 mu_hf=1.0,
                 preconditioner=False,
                 num_updates=10,
                 model_file=None):
        hf_res = self.hf_model.train(training_data,
                                     cg_data,
                                     initial_lambda=initial_lambda,
                                     mu=mu_hf,
                                     preconditioner=preconditioner,
                                     validation=valid_data,
                                     num_updates=num_updates)
        self.Win, self.W, self.Wout, self.bh, self.by = hf_res[0], hf_res[1], hf_res[2], hf_res[3], hf_res[4]

        # Optional: save model parameters
        if model_file is not None:
            save_model_parameters_theano(model_file, self)

    # Train model parameters using Gradient Descent optimization
    # NOTE: set annealing_mode to None for per-parameter adaptive learning rate methods (adagard, rmsprop, etc..)
    def sgd_train(self,
                  training_data,
                  valid_data=None,
                  grad_mode='vanilla',
                  annealing_mode=None,
                  nepoch=1000,
                  evaluate_loss_after=100,
                  model_file=None):

        losses = []  # Keep track of the losses
        for epoch in range(nepoch):

            for train_batch in training_data.iterate():
                xtrain, ytrain = train_batch[0], train_batch[1]
                if grad_mode == "vanilla":
                    self.sgd_step_vanilla(xtrain, ytrain)
                elif grad_mode == "momentum":
                    self.sgd_step_momentum(xtrain, ytrain)
                elif grad_mode == "nesterov":
                    self.sgd_step_nesterov(xtrain, ytrain)
                elif grad_mode == "adagrad":
                    self.sgd_step_adagrad(xtrain, ytrain)
                elif grad_mode == "rmsprop":
                    self.sgd_step_rmsprop(xtrain, ytrain)
                elif grad_mode == "adam":
                    self.sgd_step_adam(xtrain, ytrain)
                else:
                    print 'Invalid method string'
                    break

            # Optional: evaluate the loss on validation set
            if epoch % evaluate_loss_after == 0:
                loss = 0
                for valid_batch in valid_data.iterate():
                    xvalid, yvalid = valid_batch[0], valid_batch[1]
                    loss += self.calculate_loss(xvalid, yvalid)
                loss /= len(valid_data.items)
                losses.append(loss)
                _time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
                print "%s: Loss at epoch=%d: %f" % (_time, epoch, loss)

                # Optional: adjust the learning rate
                self.learning_rate = annealing(self.learning_rate, losses, mode=annealing_mode)

                if self.learning_rate <= 0.0001:
                    print "Learning rate too low, training aborted"
                    break

            # Optional: save model parameters
            if model_file is not None:
                save_model_parameters_theano(model_file, self)
