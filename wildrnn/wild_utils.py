# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import numpy as np
import theano.tensor as T


def softmax(x):
    xt = np.exp(x - np.max(x))
    return xt / np.sum(xt)


# Mean Squared Error
def mse(output, target):
    series_length = T.max(output.shape)
    return T.sum((output - target) ** 2) / series_length


# Normalized Rooted Mean Squared Error
def nrmse(output, target):
    output_var = T.var(output)
    return T.sqrt(mse(output, target) / output_var)


# Add a regularization term to the norm function. The possible options can be selected by setting the lambda values:
# 'reg_lamda1' = 0 and 'reg_lambda2' = 0 --> no regularization (default)
# 'reg_lamda1' != 0 and 'reg_lambda2' = 0 --> LASSO regularization
# 'reg_lamda1' = 0 and 'reg_lambda2' != 0 --> L2 regularization
# 'reg_lamda1' != 0 and 'reg_lambda2' != 0 --> elastic net penalty
def regularize(params, reg_lambda1=0, reg_lambda2=0):
    loss_reg = 0
    for p in params:
        loss_reg += reg_lambda1 * T.sum(T.abs_(p)) + reg_lambda2 / 2 * T.sum(T.pow(p, 2))
    return loss_reg


# Save model parameters to 'outfile'
def save_model_parameters_theano(outfile, model):
    W_in, W_out, W, bh, by = model.W_in.get_value(), model.W_out.get_value(), model.W.get_value(), model.bh.get_value(), model.by.get_value()
    np.savez(outfile, W_in=W_in, W_out=W_out, W=W, bh=bh, by=by)
    print "Saved model parameters to %s." % outfile


# Load model parameters from 'path'
def load_model_parameters_theano(path, model):
    npzfile = np.load(path)
    W_in, W_out, W, bh, by = npzfile["W_in"], npzfile["W_out"], npzfile["W"], npzfile["bh"], npzfile["by"]
    model.hidden_dim = W_in.shape[0]
    model.input_dim = W_in.shape[1]
    model.output_dim = W_out.shape[0]
    model.W_in.set_value(W_in)
    model.W_out.set_value(W_out)
    model.W.set_value(W)
    model.bh.set_value(bh)
    model.by.set_value(by)
    print "Loaded model parameters from %s. hidden_dim=%d input_dim=%d output_dim=%d" % (path, W_in.shape[0], W_in.shape[1], W_out.shape[0])
    print "Attenzione: parametri funzioni di update addizionali non caricati (TODO?)"


# Set of methods for annealing the learning rate.
# If mode=None, the learning rate is returned unmodified
def annealing(learning_rate, losses, mode=None, a0_ann=0.1, k_ann=1.1):

    # At each iteration halve the learning rate
    if mode == 'step1':
        learning_rate *= 0.5
        print "Learning rate %f" % learning_rate

    # If the learning rate has not improved enough since the last check, halve it
    elif mode == 'step2':
        if len(losses) > 1 and (losses[-2] - losses[-1]) <= 0.00001:
            learning_rate *= 0.5
            print "Learning rate %f" % learning_rate

    # The learning rate decays exponentially at each iteration
    elif mode == 'exp':
        learning_rate = a0_ann*np.exp(-k_ann*len(losses))
        print "Learning rate %f" % learning_rate

    # The learning rate decays proportionally with the number of iterations
    elif mode == 'inv':
        learning_rate = a0_ann/(1 + k_ann*len(losses))
        print "Learning rate %f" % learning_rate

    return learning_rate


# Generates and shuffles batches of the dataset, which can be accessed using the method iterate.
class SequenceDataset:

    # data: list of N instances of K tensor variables
    #       The data must be provided in the following form:
    #       [ [tensor_11, .., tensor_1N], [tensor_21, .., tensor_2N], .., [tensor_K1, .., tensor_KN] ]
    #       where N is the number of samples of the dataset
    # batch_size: int or None
    #       If an int, the mini-batches will be further split in chunks of length `batch_size`.
    #       This is useful for slicing subsequences or provide the full dataset in a single tensor to be split here.
    #       All tensors in `data` must then have the same leading dimension N.
    def __init__(self, data, batch_size=None):

        self.current_batch = 0
        self.items = []

        # The data are split in M batches of length batch_size, where M = N/batch_size
        # The structure of items is the following:
        # [ [batch_tensor_11, batch_tensor_21, .., batch_tensor_K1], ..,  [batch_tensor_1M, batch_tensor_2M, .., batch_tensor_KM] ]
        for i_sequence in xrange(len(data[0])):
            if batch_size is None:
                self.items.append([data[i][i_sequence] for i in xrange(len(data))])
            else:
                for i_step in xrange(0, len(data[0][i_sequence]), batch_size):
                    self.items.append([data[i][i_sequence][i_step:i_step + batch_size] for i in xrange(len(data))])

        self.shuffle()
        # Total number of different batches that are returned when method iterate() is called
        self.number_batches = len(self.items)

    def shuffle(self):
        np.random.shuffle(self.items)

    # Returns a generator that yelds a set of batches [batch_tensor_1i, batch_tensor_2i, .., batch_tensor_Ki].
    # If update=True, after the M-th batch is returned the update() method shuffles the order of the M batches in items.
    def iterate(self, update=True):
        for b in xrange(self.number_batches):
            yield self.items[(self.current_batch + b) % len(self.items)]
        if update:
            self.update()

    def update(self):
        if self.current_batch + self.number_batches >= len(self.items):
            self.shuffle()
            self.current_batch = 0
        else:
            self.current_batch += self.number_batches
