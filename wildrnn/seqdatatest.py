__author__ = 'enrico'


import itertools
import operator
import numpy as np
import sys
import os
import time
from datetime import datetime
from wild_utils import *
from wildrnn import RNNTheano
from scipy import stats
import matplotlib.pyplot as plt
from scipy.stats.stats import pearsonr
from hf import SequenceDataset


TR_SERIES_LEN = 12
TS_SERIES_LEN = 12
REGR = 2
REG_LAMBDA = 0.00
nn_hidden = 10  # number of hidden units
nn_input = 1  # number of input units
nn_output = 1  # number of output units
mu = float(0.9) # momentum strength. typical values: [0.5, 0.9, 0.95, 0.99]

# Generate dataset
x_series = np.random.randn(TR_SERIES_LEN+TS_SERIES_LEN, 1)
y_series = stats.zscore(np.asarray([[sum(x_series[range(max(j-REGR, 0), j)]),] for j in range(TR_SERIES_LEN+TS_SERIES_LEN)]))
x_train = x_series[:TR_SERIES_LEN, :]
y_train = y_series[:TR_SERIES_LEN, :]
x_test = x_series[TR_SERIES_LEN:, :]
y_test = y_series[TR_SERIES_LEN:, :]

#train = [[x_train], [y_train]]

train = [[np.arange(7)]]
#print train


#if it ain't broke don't fix it
gradient_dataset = SequenceDataset(train, batch_size=2)

next_elem = gradient_dataset.iterate()
for i in next_elem:
    print i
print "---"
next_elem = gradient_dataset.iterate()
for i in next_elem:
    print i
print "---"
next_elem = gradient_dataset.iterate()
for i in next_elem:
    print i
print "---"
next_elem = gradient_dataset.iterate()
for i in next_elem:
    print i
print "---"
next_elem = gradient_dataset.iterate()
for i in next_elem:
    print i












#print gradient_dataset.items