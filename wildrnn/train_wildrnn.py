# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import time
from wild_utils import *
from wildrnn import RNNTheano
from scipy import stats
import matplotlib.pyplot as plt
from hf import SequenceDataset

# Define parameters
LEARNING_RATE = float(0.01)  # Initial value of the learning rate
NEPOCH = 10000  # Number of epochs for the SGD optimization
EVAL_LOSS_AFT = 100  # Number of epochs after which the loss must be evaluated (defines the update frequency for learning rate)
OPTIMIZATION = 'SGD'  # Optimization type: SGD or HF
LOAD_FROM_MODEL = False  # Load network parameters from a previous run
if LOAD_FROM_MODEL:
    MODEL_FILE = 'MODEL.npz'  # Name of the file for saving network parameters
else:
    MODEL_FILE = None
TR_LEN = 3000  # Length of the training set
VS_LEN = 500  # Length of the validation set
TS_LEN = 500  # Length of the test set
REGR = 5  # Length of bptt
REG_LAMBDA1 = 0.0  # L1 regularization parameter
REG_LAMBDA2 = 0.000  # L2 regularization parameter
NN_HIDDEN = 10  # number of hidden units
NN_INPUT = 1  # number of input units
NN_OUTPUT = 1  # number of output units
MU = float(0.9)  # momentum, nesterov, rmsprop hyperparameter
MU2 = float(0.999)  # adam hyperparameter
P_DROP = float(1)  # dropout probability


# Generate datasets (Training, Validation, Test)
x_series = np.random.randn(TR_LEN+VS_LEN+TS_LEN, 1)
y_series = stats.zscore(np.asarray([[sum(x_series[range(max(j-REGR, 0), j)]), ] for j in range(TR_LEN+VS_LEN+TS_LEN)]))
x_train = x_series[:TR_LEN, :]
y_train = y_series[:TR_LEN, :]
x_valid = x_series[TR_LEN:TR_LEN+VS_LEN, :]
y_valid = y_series[TR_LEN:TR_LEN+VS_LEN, :]
x_test = x_series[TR_LEN+VS_LEN:, :]
y_test = y_series[TR_LEN+VS_LEN:, :]

train = [[x_train], [y_train]]
validation = [[x_valid], [y_valid]]

train_dataset = SequenceDataset(train, batch_size=100)
cg_dataset = SequenceDataset(train, batch_size=100)  # used to compute correlated gradient in HF optimization (must be similar to training data)
valid_dataset = SequenceDataset(validation, batch_size=50)


# Generate a new model or load an existing one
model = RNNTheano(NN_INPUT, NN_OUTPUT, NN_HIDDEN, bptt_truncate=REGR+1)
y_untrained = model.forward_propagation(x_test)
if LOAD_FROM_MODEL:
    load_model_parameters_theano(MODEL_FILE, model)

# setup hyperparameters
model.set_hyperpar(mu=MU, mu2=MU2, learning_rate=LEARNING_RATE, reg_lambda1=REG_LAMBDA1, reg_lambda2=REG_LAMBDA2)

# Train the model
t1 = time.time()
if OPTIMIZATION == 'HF':
    # HF optimization
    model.hf_train(train_dataset, cg_dataset, valid_dataset, num_updates=NEPOCH/50, model_file=MODEL_FILE)
elif OPTIMIZATION == 'SGD':
    # SGD optimization
    model.set_hyperpar(p_drop=P_DROP)  # set dropout probability
    model.sgd_train(train_dataset,
                    valid_dataset,
                    grad_mode='vanilla',
                    annealing_mode='step2',
                    nepoch=NEPOCH,
                    evaluate_loss_after=EVAL_LOSS_AFT,
                    model_file=MODEL_FILE)
t2 = time.time()
print 'Total training time: %f minutes' % ((t2 - t1)/60)


# Evaluate the output with the trained model, compute the error and the correlation w.r.t. the ground truth
model.set_hyperpar(p_drop=1)  # Important: restore dropout probability to 1
y_trained = model.forward_propagation(x_test)
test_err = model.calculate_loss(x_test, y_test)
corr = stats.pearsonr(y_test, y_trained)
print 'Correlation: {0:.3f}. Test error: {1:.3f}'.format(corr[0][0], float(test_err))


# plot the results
pl1 = plt.plot(y_untrained, label='untrained')
pl2 = plt.plot(y_test, label='true')
pl3 = plt.plot(y_trained, label='trained')
plt.legend()
plt.show()
