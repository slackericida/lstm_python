import numpy, sys
import theano
import theano.tensor as T
from hf import hf_optimizer, SequenceDataset
import numpy as np
from scipy import stats
TR_SERIES_LEN = 1000
TS_SERIES_LEN = 1000

x_series = np.random.randn(TR_SERIES_LEN+TS_SERIES_LEN)
y_series = stats.zscore(np.asarray([sum(x_series[range(max(j-REGR, 0), j)]) for j in range(TR_SERIES_LEN+TS_SERIES_LEN)]))
x_train = x_series[:TR_SERIES_LEN]
y_train = y_series[:TR_SERIES_LEN]
x_test = x_series[TR_SERIES_LEN:]
y_test = y_series[TR_SERIES_LEN:]

# single-layer recurrent neural network with sigmoid output, only last time-step output is significant
def simple_RNN(nh):
  Wx = theano.shared(0.2 * numpy.random.uniform(-1.0, 1.0, (1, nh)).astype(theano.config.floatX))
  Wh = theano.shared(0.2 * numpy.random.uniform(-1.0, 1.0, (nh, nh)).astype(theano.config.floatX))
  Wy = theano.shared(0.2 * numpy.random.uniform(-1.0, 1.0, (nh, 1)).astype(theano.config.floatX))
  bh = theano.shared(numpy.zeros(nh, dtype=theano.config.floatX))
  by = theano.shared(numpy.zeros(1, dtype=theano.config.floatX))
  h0 = theano.shared(numpy.zeros(nh, dtype=theano.config.floatX))
  p = [Wx, Wh, Wy, bh, by, h0]

  x = T.matrix()

  def recurrence(x_t, h_tm1):
    ha_t = T.dot(x_t, Wx) + T.dot(h_tm1, Wh) + bh
    h_t = T.tanh(ha_t)
    s_t = T.dot(h_t, Wy) + by
    return [ha_t, h_t, s_t]

  ([ha, h, activations], updates) = theano.scan(fn=recurrence, sequences=x, outputs_info=[dict(), h0, dict()])

  h = T.tanh(ha)  # so it is differentiable with respect to ha
  t = x[0, 0]
  s = activations[-1, 0]
  y = T.nnet.sigmoid(s)
  loss = -t*T.log(y + 1e-14) - (1-t)*T.log((1-y) + 1e-14)
  acc = T.neq(T.round(y), t)

  return p, [x], s, [loss, acc], h, ha


def example_RNN(hf=True):
    p, inputs, s, costs, h, ha = simple_RNN(10)

    gradient_dataset = SequenceDataset(x_train, batch_size=None, number_batches=5000)
    cg_dataset = SequenceDataset(x_train, batch_size=None, number_batches=1000)
    valid_dataset = SequenceDataset(y_train, batch_size=None, number_batches=1000)

    result = hf_optimizer(p, inputs, s, costs, 0.5*(h + 1), ha).train(gradient_dataset, cg_dataset, initial_lambda=0.5, mu=1.0, preconditioner=False, validation=valid_dataset)
    return result

res = example_RNN()
