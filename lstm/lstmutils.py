# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import numpy as np
import theano.tensor as T
import copy
import pickle
#from sklearn import preprocessing
import os
import time
import itertools

def softmax(x):
    xt = np.exp(x - np.max(x))
    return xt / np.sum(xt)
#
#
# # Mean Squared Error
# def mse(output, target):
#     series_length = T.max(output.shape)
#     return T.sum((output - target) ** 2) / series_length
#
#
# # Normalized Rooted Mean Squared Error
# def nrmse(output, target):
#     target_var = T.var(target)
#     return T.sqrt(mse(output, target) / target_var)
#

def np_nrmse(output, target):
    target_var = np.var(target)
    series_length = len(output)
    mse = np.sum((output - target) ** 2) / float(series_length)
    return np.sqrt(mse / target_var)

#
# # Add a regularization term to the norm function. The possible options can be selected by setting the lambda values:
# # 'reg_lamda1' = 0 and 'reg_lambda2' = 0 --> no regularization (default)
# # 'reg_lamda1' != 0 and 'reg_lambda2' = 0 --> LASSO regularization
# # 'reg_lamda1' = 0 and 'reg_lambda2' != 0 --> L2 regularization
# # 'reg_lamda1' != 0 and 'reg_lambda2' != 0 --> elastic net penalty
# def regularize(params, reg_lambda1=0, reg_lambda2=0):
#     loss_reg = 0
#     for p in params:
#         pdim = p.flatten().shape[0]
#         loss_reg += (reg_lambda1 * T.sum(T.abs_(p)) + reg_lambda2 / 2 * T.sum(T.pow(p, 2))) / T.sqrt(pdim)
#     return loss_reg
#
#
# # Save model parameters to 'outfile'
# def save_model_parameters_theano(file_name, model):
#     params_numpy = {k: p.get_value() for k, p in model.params.iteritems()}
#     saved_data = dict(s_hidden_dim=model.hidden_dim, s_params=params_numpy, s_net_type=model.net_type)
#     with open(file_name, 'wb') as file_w:
#         pickle.dump(saved_data, file_w, protocol=pickle.HIGHEST_PROTOCOL)
#     print "Saved model parameters to %s." % file_name
#
#
# # Load model parameters from 'path'
# def load_model_parameters_theano(file_name, model):
#     with open(file_name, 'rb') as file_r:
#         saved_data = pickle.load(file_r)
#     model.hidden_dim = saved_data["s_hidden_dim"]
#     model.net_type = saved_data["s_net_type"]
#     for key in saved_data["s_params"]:
#         model.params[key].set_value(saved_data["s_params"][key])
#     print "Loaded model parameters from %s. Network type: %s with %d neurons" % (file_name, saved_data["s_net_type"], saved_data["s_hidden_dim"])
#
#
# # Set of methods for annealing the learning rate.
# def annealing(learning_rate, losses, mode, a0_ann=0.1, k_ann=1.1):
#
#     # At each iteration halve the learning rate
#     if mode == 'step1':
#         learning_rate *= 0.5
#         print "Learning rate %f" % learning_rate
#
#     # If the learning rate has not improved enough since the last check, halve it
#     elif mode == 'step2':
#         if len(losses) > 1 and (losses[-2] - losses[-1]) <= 0.00001:
#             learning_rate *= 0.5
#             print "Learning rate %f" % learning_rate
#
#     # The learning rate decays exponentially at each iteration
#     elif mode == 'exp':
#         learning_rate = a0_ann*np.exp(-k_ann*len(losses))
#         print "Learning rate %f" % learning_rate
#
#     # The learning rate decays proportionally with the number of iterations
#     elif mode == 'inv':
#         learning_rate = a0_ann/(1 + k_ann*len(losses))
#         print "Learning rate %f" % learning_rate
#
#     # No annealing
#     elif mode is None:
#         learning_rate = learning_rate
#
#     else:
#         print "annealing: Invalid annealing method"
#         return
#
#     return learning_rate


# Normalize data in a time-series
def data_normalization(data_vector, mode):

    return (data_vector - np.mean(data_vector)) / np.std(data_vector)
    # if mode == 'rescale01': # rescaling in [0, 1]
    #     min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
    #     return min_max_scaler.fit_transform(data_vector)
    #
    # if mode == 'rescale-11': # rescaling in [-1, 1]
    #     min_max_scaler = preprocessing.MinMaxScaler(feature_range=(-1, 1))
    #     return min_max_scaler.fit_transform(data_vector)
    #
    # if mode == 'zscore':  # Z-score
    #     return preprocessing.scale(data_vector)


# Generates and shuffles batches of the dataset, which can be accessed using the method iterate.
class SequenceDataset:

    # data: list of N instances of K tensor variables
    #       The data must be provided in the following form:
    #       [ [tensor_11, .., tensor_1N], [tensor_21, .., tensor_2N], .., [tensor_K1, .., tensor_KN] ]
    #       where N is the number of samples of the dataset
    # batch_size: int or None
    #       If an int, the mini-batches will be further split in chunks of length `batch_size`.
    #       This is useful for slicing subsequences or provide the full dataset in a single tensor to be split here.
    #       All tensors in `data` must then have the same leading dimension N.
    def __init__(self, data, batch_size=None):

        self.current_batch = 0
        self.items = []

        # The data are split in M batches of length batch_size, where M = N/batch_size
        # The structure of items is the following:
        # [ [batch_tensor_11, batch_tensor_21, .., batch_tensor_K1], ..,  [batch_tensor_1M, batch_tensor_2M, .., batch_tensor_KM] ]
        for i_sequence in xrange(len(data[0])):
            if batch_size is None:
                self.items.append([data[i][i_sequence] for i in xrange(len(data))])
            else:
                for i_step in xrange(0, len(data[0][i_sequence]), batch_size):
                    self.items.append([data[i][i_sequence][i_step:i_step + batch_size] for i in xrange(len(data))])

        self.shuffle()
        # Total number of different batches that are returned when method iterate() is called
        self.number_batches = len(self.items)

    def shuffle(self):
        np.random.shuffle(self.items)

    # Returns a generator that yelds a set of batches [batch_tensor_1i, batch_tensor_2i, .., batch_tensor_Ki].
    # If update=True, after the M-th batch is returned the update() method shuffles the order of the M batches in items.
    def iterate(self, update=True):
        for b in xrange(self.number_batches):
            yield self.items[(self.current_batch + b) % len(self.items)]
        if update:
            self.update()

    def update(self):
        if self.current_batch + self.number_batches >= len(self.items):
            self.shuffle()
            self.current_batch = 0
        else:
            self.current_batch += self.number_batches

class Scheduler:

    def __init__(self, model, fn, save_filename = None):
        self.model = model
        self.fn = fn
        self.fn_calls = []
        self.curr_call = 0
        self.save_filename = save_filename

    def schedule(self, *args, **kwargs):
        self.fn_calls.append([args, kwargs])

    def execute_next(self):
        self.model.init_network()

        fn_call = self.fn_calls.pop(0)
        args = fn_call[0]
        kwargs = fn_call[1]

        print "Scheduler: Executing function call with args: "
        print args
        print "and kwargs: "
        print kwargs
        fn_output = self.fn(*args, **kwargs)
        if self.save_filename != None:
            self.save_schedule()
        self.curr_call += 1
        return fn_output

    def execute_all(self):
        tot_calls = len(self.fn_calls)
        fn_outputs = []
        t0 = time.time()
        while len(self.fn_calls) > 0:
            print "Scheduler: Iteration ", self.curr_call+1, "of ", tot_calls
            t1 = time.time()
            fn_outputs.append(self.execute_next())
            t2 = time.time()
            print "Scheduler: Iteration complete. Time elapsed: ", str(t2-t1)
            print "------------------------------"
        t3 = time.time()
        print "Scheduler: Multi-schedule complete. Total time elapsed: ", t3-t0
        if self.save_filename != None:
            os.remove(self.save_filename) # delete (now empty) snapshot file
        return fn_outputs

    def multi_schedule(self, variable_index, *args, **kwargs):
        if isinstance(variable_index, int):
            for i in range(len(args[variable_index])):
                newargs = list(args)
                newargs[variable_index] = args[variable_index][i]
                self.schedule(*newargs, **kwargs)
        elif isinstance(variable_index, basestring):
            for i in range(len(kwargs[variable_index])):
                newkwargs = kwargs.copy()
                newkwargs[variable_index] = kwargs[variable_index][i]
                self.schedule(*args, **newkwargs)
        elif isinstance(variable_index, list):
            vars = [var_ind for i in variable_index for var_ind in i]
            vars_dict = {}
            for v in vars:
                if isinstance(v, int):
                    vars_dict[v] = args[v]
                else:
                    vars_dict[v] = kwargs[v]
            arglists = itertools.product(*[zip(*[[[a,e] for e in vars_dict[a]] for a in arg]) for arg in variable_index])
            for arglist in arglists:
                f_arglist = [elem for temp in arglist for elem in temp] # flatten arglist array
                newargs = list(args)
                newkwargs = kwargs.copy()
                for arg in f_arglist:
                    if isinstance(arg[0], int):
                        newargs[arg[0]] = arg[1]
                    elif isinstance(arg[0], basestring):
                        newkwargs[arg[0]] = arg[1]
                self.schedule(*newargs, **newkwargs)
        else:
            raise ValueError("Invalid variable_index provided.")

    def save_schedule(self):
        with open(self.save_filename, 'wb') as file_w:
            pickle.dump(self.fn_calls, file_w, protocol=pickle.HIGHEST_PROTOCOL)

    def load_schedule(self, filename):
        with open(filename, 'rb') as file_r:
            self.fn_calls = pickle.load(file_r)

class RNNLogger:

    def __init__(self, filename, log_params, newfile = False, models_folder = None):
        self.filename = filename
        if newfile:
            if os.path.exists(filename):
                resp = raw_input("RNNLogger: A file named " + filename + " already exists. Do you want to:\n(o) Overwrite,\n(a) Append,\n(h) Append with new headers?")
                if resp == "o":
                    filewrite = "w"
                elif resp == "a" or resp == "h":
                    filewrite = "a"
            else:
                filewrite = "w"
            with open(filename, filewrite) as file:
                if filewrite == "w" or resp == "h":
                    file.write("DateID\t" + "\t".join([p[0] for p in log_params]) + "\n") # creates the variable titles in the logfile
        self.log_params = log_params
        self.models_folder = models_folder


    def hook(self, model, event, opt_data = None):
        if event == "ev_end":
            date_id = time.strftime("%Y-%m-%d-%H-%M-%S")
            with open(self.filename, "a") as file:
                file.write(date_id + "\t")
                for p in self.log_params:
                    if len(p) == 1:  # if the log_param is given in the ["param_name"] format
                        value = opt_data[p[0]]
                    else: # if the log_param is given in the ["param_title", "param_name"] format
                        if callable(p[1]): # if it is a lambda function
                            value = p[1](model)
                        elif isinstance(p[1], basestring):
                            value = opt_data[p[1]]
                        else:
                            raise ValueError("Invalid log param provided.")
                    file.write(str(value) + "\t")
                file.write("\n")
            if not self.models_folder == None:
                model.save_model_parameters_theano(self.models_folder + date_id + ".pickle")



