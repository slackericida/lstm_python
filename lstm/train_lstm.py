# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import time
from lstmutils import *
from lstmef import LSTMTheano
from scipy import stats
import matplotlib.pyplot as plt
from hf import SequenceDataset

# Define parameters
LEARNING_RATE = float(0.01)  # Initial value of the learning rate
NEPOCH = 100  # Number of epochs for the SGD optimization
EVAL_LOSS_AFT = 100  # Number of epochs after which the loss must be evaluated (defines the update frequency for learning rate)
OPTIMIZATION = 'SGD'  # Optimization type: SGD or HF
GRAD_MODE = 'vanilla'
ANNEAL_MODE = 'step2'
NET_TYPE = 'RNN'
LOAD_FROM_MODEL = True  # Load network parameters from a previous run
MODEL_FILE = 'MODEL.npz'  # Name of the file for saving network parameters
VS_PERC = 0.15  # Percentage of dataset used as validation
TS_PERC = 0.15  # Percentage of dataset used as test
REGR = 10  # Length of bptt
REG_LAMBDA1 = 0.01  # L1 regularization parameter
REG_LAMBDA2 = 0.0  # L2 regularization parameter
NN_HIDDEN = 15  # number of hidden units
NN_INPUT = 1  # number of input units
NN_OUTPUT = 1  # number of output units
MU = float(0.9)  # momentum, nesterov, rmsprop hyperparameter
MU2 = float(0.999)  # adam hyperparameter
P_DROP = 1.0  # dropout probability (probability of keeping a neuron active)
TS_FILENAME = "MG"

# Load dataset
ts_dataset = np.loadtxt(fname='../Dataset/' + TS_FILENAME, delimiter='\t')
print 'Dataset read: {0:d} rows and {1:d} column'.format(ts_dataset.shape[0], ts_dataset.shape[1])
ts_dataset = data_normalization(ts_dataset, 'zscore')
dataset_length = ts_dataset.shape[0]
vs_length = int(dataset_length * VS_PERC)
ts_length = int(dataset_length * TS_PERC)
obsData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 0]).T
futureData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 1]).T
obsData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 0]).T
futureData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 1]).T
obsData_ts = np.atleast_2d(ts_dataset[-ts_length:, 0]).T
futureData_ts = np.atleast_2d(ts_dataset[-ts_length:, 1]).T
train_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)
cg_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)  # used to compute correlated gradient in HF optimization (must be similar to training data)
valid_dataset = SequenceDataset([[obsData_vs], [futureData_vs]], batch_size=vs_length)


# Generate a new model or load an existing one
model = LSTMTheano(NN_INPUT, NN_OUTPUT, NN_HIDDEN, NET_TYPE, bptt_truncate=REGR)
y_untrained = model.forward_propagation(obsData_ts)
if LOAD_FROM_MODEL:
    model.load_model_parameters_theano(MODEL_FILE)


# Train the model
t1 = time.time()
losses = []
if OPTIMIZATION == 'HF':  # HF optimization
    model.hf_train(train_dataset, cg_dataset, valid_dataset, num_updates=NEPOCH/50, model_file=MODEL_FILE)
elif OPTIMIZATION == 'SGD':  # SGD optimization
    losses = model.sgd_train(train_dataset, valid_dataset, grad_mode=GRAD_MODE, annealing_mode=ANNEAL_MODE,
                             nepoch=NEPOCH, evaluate_loss_after=EVAL_LOSS_AFT, learning_rate=LEARNING_RATE,
                             lambda1=REG_LAMBDA1, lambda2=REG_LAMBDA2, p_drop=P_DROP, mu=MU, mu2=MU2, model_file=MODEL_FILE)
t2 = time.time()
print 'Total training time: %f minutes' % ((t2 - t1)/60)

# Evaluate the output with the trained model, compute the error and the correlation w.r.t. target values
model.load_model_parameters_theano(MODEL_FILE)  # load the best set of parameters found
y_trained = model.predict(obsData_ts)
test_err = np_nrmse(y_trained, futureData_ts)
corr = stats.pearsonr(futureData_ts, y_trained)
print 'Correlation: {0:.3f}. Test error: {1:.3f}'.format(corr[0][0], float(test_err))
#np.savetxt('Results_'+NET_TYPE+'_'+OPTIMIZATION,y_trained)

# plot the results
pl1 = plt.plot(y_untrained, label='untrained')
pl2 = plt.plot(futureData_ts, label='true')
pl3 = plt.plot(y_trained, label='trained')
plt.legend()
plt.show()

plt.plot(losses, label='loss')
plt.show()
