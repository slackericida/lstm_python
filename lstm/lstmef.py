# Artists:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from theano.ifelse import ifelse
from lstmutils import *
from hf import hf_optimizer
from datetime import datetime
#theano.config.exception_verbosity = "high"
theano.config.optimizer = "fast_compile"  # usefull for debug
#theano.config.optimizer = "fast_run"  # production
# theano.config.optimizer = "None"
#theano.config.blas.ldflags = "" # To uncomment if BLAS implementation does not work (falls back to numpy BLAS)


class LSTMTheano:

    def __init__(self, input_dim, output_dim, hidden_dim, net_type, bptt_truncate):
        # Assign instance variables
        self.hidden_dim = hidden_dim
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.bptt_truncate = bptt_truncate
        self.CFdim = 20
        self.CSdim = 20
        self.t1 = 1
        self.t2 = 15
        self.t3 = 47

        # Type of network to be used (RNN, LSTM, GRU)
        self.net_type = net_type

        self.forward_prop_step_fn = None
        self.params_list = []
        self.regularization_list = []
        if self.net_type == 'RNN':
            self.params_list = ["Wz","Wv","Rz","bz","bv"]
            self.regularization_list = ["Wz", "Rz", "Wv"]
        elif self.net_type == 'LSTM':
            self.params_list = ["Wz", "Wi", "Wf", "Wo", "Wv", "Rz", "Ri", "Rf", "Ro", "pi", "pf", "po", "bz", "bi", "bf", "bo", "bv"]
            self.regularization_list = ["Wz", "Wi", "Wf", "Wo", "Rz", "Ri", "Rf", "Ro", "Wv"]
        elif self.net_type == 'GRU':
            self.params_list = ["Wz", "Wi", "Wf", "Wv", "Rz", "Ri", "Rf", "bz", "bi", "bf", "bv"]
            self.regularization_list = ["Wz", "Wi", "Wf", "Rz", "Ri", "Rf", "Wv"]
        elif self.net_type == 'MTRNN':
            print "Hello I am a MTRNN network"
            self.params_list = ["Wi1", "W11", "W1o", "W12", "W13", "W21", "W22", "W23", "W31", "W32", "W33", "bi1", "b1o", "b12", "b13", "b21", "b22", "b23", "b31", "b32", "b33" ]
            self.regularization_list = ["Wi1", "W1o", "W12", "W13", "W21", "W22", "W23", "W31", "W32", "W33"]
        # ---------- SHARED VARIABLES ----------

        initparams = self.get_initparams()

        # Network parameters (weight matrices and biases)
        self.params = {}
        for key in self.params_list:
            self.params[key] = theano.shared(name=key, value = initparams[key].astype(theano.config.floatX))

        # Parameters used by some SGD strategies
        self.sgd_update_params_1 = {}
        self.sgd_update_params_2 = {}
        for key in self.params_list:
            self.sgd_update_params_1[key+"_1"] = theano.shared(name=key + "_1", value = np.zeros(initparams[key].shape).astype(theano.config.floatX))
            self.sgd_update_params_2[key+"_2"] = theano.shared(name=key + "_2", value = np.zeros(initparams[key].shape).astype(theano.config.floatX))


        # WARNING: these values are set on default values since they are overwritten when sgd_train function is called
        # current iteration
        self.iteration = theano.shared(name='curr iteration', value = 1)

        self.p_drop = theano.shared(name='p_drop', value = 1.0)
        self.learning_rate = theano.shared(name='learning_rate', value=0.01)
        self.reg_lambda1 = theano.shared(name='reg_lambda1', value = 0.0)
        self.reg_lambda2 = theano.shared(name='reg_lambda2', value = 0.0)

        # gradient update hyperparameters
        self.mu = theano.shared(name='mu', value = 0.9)
        self.mu2 = theano.shared(name='mu2', value = 0.999)

        self.init_network()
        # We store the Theano graph here
        # self.theano = {}
        self.__theano_build__()

        self.events_cbs = []

    def init_network(self):
        initparams = self.get_initparams()

        for key in self.params_list:
            self.params[key].set_value(initparams[key].astype(theano.config.floatX))

        for key in self.params_list:
            self.sgd_update_params_1[key+"_1"].set_value(np.zeros(initparams[key].shape).astype(theano.config.floatX))
            self.sgd_update_params_2[key+"_2"].set_value(np.zeros(initparams[key].shape).astype(theano.config.floatX))

    def get_initparams(self):

        input_dim = self.input_dim
        output_dim = self.output_dim
        hidden_dim = self.hidden_dim
        CFdim = self.CFdim
        CSdim = self.CSdim

        # Randomly initialize the network parameters
        initparams = dict()
        # input weights
        initparams["Wz"] = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (hidden_dim, input_dim))
        initparams["Wi"] = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (hidden_dim, input_dim))
        initparams["Wf"] = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (hidden_dim, input_dim))
        initparams["Wo"] = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (hidden_dim, input_dim))
        # recurrent weights
        initparams["Rz"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        initparams["Ri"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        initparams["Rf"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        initparams["Ro"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        # peephole weights
        initparams["pi"] = np.zeros(hidden_dim)
        initparams["pf"] = np.zeros(hidden_dim)
        initparams["po"] = np.zeros(hidden_dim)
        # bias weights
        initparams["bz"] = np.zeros(hidden_dim)
        initparams["bi"] = np.zeros(hidden_dim)
        initparams["bf"] = np.zeros(hidden_dim)
        initparams["bo"] = np.zeros(hidden_dim)
        # output weights
        initparams["Wv"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (output_dim, hidden_dim))
        initparams["bv"] = np.zeros(output_dim)

        #MTRNN
        initparams["Wi1"] = np.random.uniform(-np.sqrt(1./input_dim), np.sqrt(1./input_dim), (hidden_dim, input_dim))
        initparams["W11"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        initparams["W1o"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (output_dim, hidden_dim))
        initparams["W12"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (CFdim, hidden_dim))
        initparams["W13"] = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (CSdim, hidden_dim))
        initparams["W21"] = np.random.uniform(-np.sqrt(1./CFdim), np.sqrt(1./CFdim), (hidden_dim, CFdim))
        initparams["W22"] = np.random.uniform(-np.sqrt(1./CFdim), np.sqrt(1./CFdim), (CFdim, CFdim))
        initparams["W23"] = np.random.uniform(-np.sqrt(1./CFdim), np.sqrt(1./CFdim), (CSdim, CFdim))
        initparams["W31"] = np.random.uniform(-np.sqrt(1./CSdim), np.sqrt(1./CSdim), (hidden_dim, CSdim))
        initparams["W32"] = np.random.uniform(-np.sqrt(1./CSdim), np.sqrt(1./CSdim), (CFdim, CSdim))
        initparams["W33"] = np.random.uniform(-np.sqrt(1./CSdim), np.sqrt(1./CSdim), (CSdim, CSdim))

        initparams["bi1"] = np.zeros(hidden_dim).astype(theano.config.floatX)
        initparams["b1o"] = np.zeros(output_dim).astype(theano.config.floatX)
        initparams["b12"] = np.zeros(CFdim).astype(theano.config.floatX)
        initparams["b13"] = np.zeros(CSdim).astype(theano.config.floatX)
        initparams["b21"] = np.zeros(hidden_dim).astype(theano.config.floatX)
        initparams["b22"] = np.zeros(CFdim).astype(theano.config.floatX)
        initparams["b23"] = np.zeros(CSdim).astype(theano.config.floatX)
        initparams["b31"] = np.zeros(hidden_dim).astype(theano.config.floatX)
        initparams["b32"] = np.zeros(CFdim).astype(theano.config.floatX)
        initparams["b33"] = np.zeros(CSdim).astype(theano.config.floatX)

        return initparams

    def __theano_build__(self):

        # Initialize variables
        x = T.matrix('input vec')
        y = T.matrix('target_vec')
        rng = RandomStreams()
        masks = rng.binomial(size=[x.shape[0], self.output_dim, self.hidden_dim], p=self.p_drop).astype(theano.config.floatX) / self.p_drop
        # print masks.eval({x:np.ones([10,1])})

        # LSTM forward propagation
        def lstm_forward_prop_step(x_t, mask, h_t_prev, c_t_prev, dummy2):
            z_t = T.tanh(T.dot(self.params["Wz"], x_t) + T.dot(self.params["Rz"], h_t_prev) + self.params["bz"])
            i_t = T.nnet.sigmoid(T.dot(self.params["Wi"], x_t) + T.dot(self.params["Ri"],h_t_prev) + self.params["pi"] * c_t_prev + self.params["bi"])
            f_t = T.nnet.sigmoid(T.dot(self.params["Wf"], x_t) + T.dot(self.params["Rf"],h_t_prev) + self.params["pf"] * c_t_prev + self.params["bf"])
            c_t = i_t * z_t + f_t * c_t_prev
            o_t = T.nnet.sigmoid(T.dot(self.params["Wo"], x_t) + T.dot(self.params["Ro"],h_t_prev) + self.params["po"] * c_t + self.params["bo"])
            h_t = o_t * T.tanh(c_t)
            v_t = ifelse(T.lt(self.p_drop, 1.0), T.dot(self.params["Wv"] * mask, h_t) + self.params["bv"], T.dot(self.params["Wv"], h_t) + self.params["bv"])
            return v_t, h_t, c_t, dummy2

        # GRU forward propagation -- c_t_prev is just for signature compatibility
        def gru_forward_prop_step(x_t, mask, h_t_prev, dummy1, dummy2):
            # in Cho et al. 2014
            # z -> r (reset gate)
            # i -> z (update gate)
            # f -> h tilde
            z_t = T.nnet.sigmoid(T.dot(self.params["Wz"], x_t) + T.dot(self.params["Rz"], h_t_prev) + self.params["bz"])
            i_t = T.nnet.sigmoid(T.dot(self.params["Wi"], x_t) + T.dot(self.params["Ri"],h_t_prev) + self.params["bi"])
            f_t = T.tanh(T.dot(self.params["Wf"], x_t) + T.dot(self.params["Rf"], z_t * h_t_prev) + self.params["bf"])
            h_t = i_t * h_t_prev + (1-i_t) * f_t
            v_t = ifelse(T.lt(self.p_drop, 1.0), T.dot(self.params["Wv"] * mask, h_t) + self.params["bv"], T.dot(self.params["Wv"], h_t) + self.params["bv"])
            return v_t, h_t, dummy1, dummy2

        # RNN forward propagation -- c_t_prev is just for signature compatibility
        def rnn_forward_prop_step(x_t, mask, h_t_prev, dummy1, dummy2):
            h_t = T.tanh(T.dot(self.params["Wz"], x_t) + T.dot(self.params["Rz"], h_t_prev) + self.params["bz"])
            v_t = ifelse(T.lt(self.p_drop, 1.0), T.dot(self.params["Wv"] * mask, h_t) + self.params["bv"], T.dot(self.params["Wv"], h_t) + self.params["bv"])
            return v_t, h_t, dummy1, dummy2

        """
        # MTRNN forward propagation -- c_t_prev is just for signature compatibility
        def mtrnn_forward_prop_step(x_t, mask, h1_t_prev, h2_t_prev, h3_t_prev):
            print "Hello I am still a MTRNN network"
            h1 = (1-1.0/self.t1) * h1_t_prev + (1.0/self.t1) * T.dot(self.params["Wi1"], x_t) + (1.0/self.t1) * T.dot(self.params["W21"], T.nnet.sigmoid(h2_t_prev)) + 0 * T.dot(self.params["W31"], T.nnet.sigmoid(h3_t_prev)) + 0 * (self.params["bi1"] + self.params["b21"] + self.params["b31"])
            h2 = (1-1.0/self.t2) * h2_t_prev + (1.0/self.t1) * T.dot(self.params["W12"], T.tanh(h1_t_prev)) + 0 * T.dot(self.params["W22"], T.nnet.sigmoid(h2_t_prev)) + 0 * T.dot(self.params["W32"], T.nnet.sigmoid(h3_t_prev)) + 0 * (self.params["b12"] + self.params["b22"] + self.params["b32"])
            h3 = (1-1.0/self.t3) * h3_t_prev + 0 * T.dot(self.params["W13"], T.nnet.sigmoid(h1_t_prev)) + 0 * T.dot(self.params["W23"], T.nnet.sigmoid(h2_t_prev)) + 0 * T.dot(self.params["W33"], T.nnet.sigmoid(h3_t_prev)) + 0 * (self.params["b13"] + self.params["b23"] + self.params["b33"])
            v1 = T.tanh(T.dot(self.params["W1o"], h1) + self.params["b1o"])

            #v1 = T.tanh(T.dot(self.params["W1o"], h1_t_prev) + self.params["b1o"])
            #v2 = T.nnet.sigmoid(T.dot(self.params["W22"], h2_t_prev) + self.params["b22"])
            #v3 = T.nnet.sigmoid(T.dot(self.params["W33"], h3_t_prev) + self.params["b33"])

            #h1 = (1-1/self.t1) * h1_t_prev + (1/self.t1) * (v2 + v3 + T.dot(self.params["Wi1"], x_t))
            #h2 = (1-1/self.t2) * h2_t_prev + (1/self.t2) * (v1 + v2 + v3)
            #h3 = (1-1/self.t3) * h3_t_prev + (1/self.t3) * (v1 + v2 + v3)
 
            return v1, h1, h2, h3
        """

        def mtrnn_forward_prop_step(x_t, mask, h1_t_prev, h2_t_prev, h3_t_prev):
            print "Hello I am still a MTRNN network"
            h1 = T.tanh((1-1.0/self.t1) * h1_t_prev + (1.0/self.t1) * (T.dot(self.params["Wi1"], x_t) + T.dot(self.params["W11"], h1_t_prev)))# + self.params["bi1"] + self.params["b21"] + self.params["b31"])
            #h1 = T.tanh((1-1.0/self.t1) * h1_t_prev + (1.0/self.t1) * (T.dot(self.params["Wi1"], x_t) + T.dot(self.params["W21"], h2_t_prev) + T.dot(self.params["W31"], h3_t_prev)) + self.params["bi1"] + self.params["b21"] + self.params["b31"])
            h2 =T.tanh((1-1.0/self.t2) * h2_t_prev + (1.0/self.t2) * (T.dot(self.params["W12"], h1_t_prev) + T.dot(self.params["W22"], h2_t_prev) +T.dot(self.params["W32"], h3_t_prev)) + self.params["b12"] + self.params["b22"] + self.params["b32"])
            h3 = T.tanh((1-1.0/self.t3) * h3_t_prev + (1.0/self.t3) * (T.dot(self.params["W13"], h1_t_prev) + T.dot(self.params["W23"], h2_t_prev) + T.dot(self.params["W33"], h3_t_prev)) + self.params["b13"] + self.params["b23"] + self.params["b33"])


            v1 = T.dot(self.params["W1o"], h1) + self.params["b1o"]

            #v1 = T.tanh(T.dot(self.params["W1o"], h1_t_prev) + self.params["b1o"])
            #v2 = T.nnet.sigmoid(T.dot(self.params["W22"], h2_t_prev) + self.params["b22"])
            #v3 = T.nnet.sigmoid(T.dot(self.params["W33"], h3_t_prev) + self.params["b33"])

            #h1 = (1-1/self.t1) * h1_t_prev + (1/self.t1) * (v2 + v3 + T.dot(self.params["Wi1"], x_t))
            #h2 = (1-1/self.t2) * h2_t_prev + (1/self.t2) * (v1 + v2 + v3)
            #h3 = (1-1/self.t3) * h3_t_prev + (1/self.t3) * (v1 + v2 + v3)
 
            return v1, h1, h2, h3
            
            
        hidden_dim2 = self.hidden_dim
        hidden_dim3 = self.CSdim
        forward_prop_step_fn = None
        if self.net_type == 'RNN':
            forward_prop_step_fn = rnn_forward_prop_step
        elif self.net_type == 'LSTM':
            forward_prop_step_fn = lstm_forward_prop_step
        elif self.net_type == 'GRU':
            forward_prop_step_fn = gru_forward_prop_step
        elif self.net_type == 'MTRNN':
            forward_prop_step_fn = mtrnn_forward_prop_step
            hidden_dim2 = self.CFdim

        [v, _, _, _], _ = theano.scan(
            forward_prop_step_fn,
            sequences=[x, masks],
            outputs_info=[None, dict(initial=np.zeros(self.hidden_dim)), dict(initial=np.zeros(hidden_dim2).astype(theano.config.floatX)), dict(initial=np.zeros(hidden_dim3).astype(theano.config.floatX))],
            truncate_gradient=self.bptt_truncate,
            strict=False)


        # Loss function
        loss_reg = self.regularize([self.params[key] for key in self.regularization_list], reg_lambda1=self.reg_lambda1, reg_lambda2=self.reg_lambda2)
        loss_err = self.nrmse(v, y)
        v_error = loss_err + loss_reg

        # Gradients
        gradients = {}
        for key in self.params:
            #print key
            #print self.params[key].eval().shape
            gradients["d"+key] = T.grad(v_error, self.params[key], add_names=True, disconnected_inputs='warn')
        gradients_1 = {}
        for key in self.params:
            gradients_1["d"+key+"_1"] = T.grad(v_error, self.sgd_update_params_1[key+"_1"], add_names=True, disconnected_inputs='warn')

        # Theano functions
        self.forward_propagation = theano.function(inputs=[x], outputs=v)
        self.calculate_loss = theano.function(inputs=[x, y], outputs=v_error)
        self.bptt = theano.function(inputs=[x, y], outputs=gradients.values())

        # Vanilla gradient update
        sgd_step_vanilla_update = []
        for key in self.params:
            sgd_step_vanilla_update.append((self.params[key],self.params[key] - self.learning_rate * gradients["d"+key]))
        self.sgd_step_vanilla = theano.function(inputs=[x, y],
                                                outputs=[],
                                                updates=sgd_step_vanilla_update)

        # Momentum gradient update
        sgd_step_momentum_update = []
        # integrate velocity
        for key in self.params:
            sgd_step_momentum_update.append((self.sgd_update_params_1[key+"_1"], self.mu * self.sgd_update_params_1[key+"_1"] - self.learning_rate * gradients["d"+key]))
        # integrate position
        for key in self.params:
            sgd_step_momentum_update.append((self.params[key],self.params[key] + self.sgd_update_params_1[key+"_1"]))
        self.sgd_step_momentum = theano.function(inputs=[x, y],
                                                 outputs=[],
                                                 updates=sgd_step_momentum_update)

        # Nesterov gradient update
        sgd_step_nesterov_update = []
        # integrate velocity
        for key in self.params:
            sgd_step_nesterov_update.append((self.sgd_update_params_1[key+"_1"], self.mu * self.sgd_update_params_1[key+"_1"] - self.learning_rate * (gradients["d"+key]+gradients_1["d"+key+"_1"])))
        # integrate position
        for key in self.params:
            sgd_step_nesterov_update.append((self.params[key], self.params[key] + self.sgd_update_params_1[key+"_1"]))
        self.sgd_step_nesterov = theano.function(inputs=[x, y],
                                                 outputs=[],
                                                 updates=sgd_step_nesterov_update)

        # Adagrad gradient update
        sgd_step_adagrad_update = []
        for key in self.params:
            sgd_step_adagrad_update.append((self.sgd_update_params_1[key+"_1"], self.sgd_update_params_1[key+"_1"] + gradients["d"+key]**2))
        for key in self.params:
            sgd_step_adagrad_update.append((self.params[key], self.params[key] - self.learning_rate * gradients["d"+key] / T.sqrt(self.sgd_update_params_1[key+"_1"] + 1e-08)))
        self.sgd_step_adagrad = theano.function(inputs=[x, y],
                                                outputs=[],
                                                updates=sgd_step_adagrad_update)

        # RMSprop gradient update (damped Adagrad)
        sgd_step_rmsprop_update = []
        for key in self.params:
            sgd_step_rmsprop_update.append((self.sgd_update_params_1[key+"_1"], self.mu * self.sgd_update_params_1[key+"_1"] + (1-self.mu) * gradients["d"+key]**2))
        for key in self.params:
            sgd_step_rmsprop_update.append((self.params[key], self.params[key] - self.learning_rate * gradients["d"+key] / T.sqrt(self.sgd_update_params_1[key+"_1"] + 1e-8)))
        self.sgd_step_rmsprop = theano.function(inputs=[x, y],
                                                outputs=[],
                                                updates=sgd_step_rmsprop_update)

        # Adam gradient update
        sgd_step_adam_update = []
        for key in self.params:
            sgd_step_adam_update.append((self.sgd_update_params_1[key+"_1"], self.mu * self.sgd_update_params_1[key+"_1"] + (1-self.mu) * gradients["d"+key]))
        for key in self.params:
            sgd_step_adam_update.append((self.sgd_update_params_2[key+"_2"], self.mu2 * self.sgd_update_params_2[key+"_2"] + (1-self.mu2) * gradients["d"+key]**2))
        sgd_step_adam_update.append((self.iteration, self.iteration + 1))
        for key in self.params:
            sgd_step_adam_update.append((self.params[key],self.params[key] - self.learning_rate * T.sqrt(1-self.mu2**self.iteration) / (1-self.mu**self.iteration) * self.sgd_update_params_1[key+"_1"] / (T.sqrt(self.sgd_update_params_2[key+"_2"])+1e-8)))
        self.sgd_step_adam = theano.function(inputs=[x, y],
                                             outputs=[],
                                             updates=sgd_step_adam_update)

        # Hessian Free optimization
        # Arguments: model_parameters, inputs, output_hidden_layer, loss_function
        self.hf_model = hf_optimizer(self.params.values(), [x, y], v, [v_error])

    def predict(self, ts):
        prev_p_drop = self.p_drop.get_value()
        self.p_drop.set_value(1.0)
        predicted = self.forward_propagation(ts)
        self.p_drop.set_value(prev_p_drop)
        return predicted

    # Train model parameters using Hessian Free optimization
    def hf_train(self,
                 training_data,
                 cg_data,
                 valid_data,
                 num_updates,
                 initial_lambda=0.5,
                 mu_hf=1.0,
                 preconditioner=False,
                 model_file=None):
        hf_res = self.hf_model.train(training_data,
                                     cg_data,
                                     initial_lambda=initial_lambda,
                                     mu=mu_hf,
                                     preconditioner=preconditioner,
                                     validation=valid_data,
                                     num_updates=num_updates)
        for param in hf_res[2]:
            self.params[param.name].set_value(param.get_value())

        # Optional: save model parameters
        if model_file is not None:
            self.save_model_parameters_theano(model_file)

    # Train model parameters using Gradient Descent optimization
    # NOTE: set annealing_mode to None for per-parameter adaptive learning rate methods (adagard, rmsprop, etc..)
    def sgd_train(self,
                  training_data,
                  valid_data,
                  grad_mode,
                  annealing_mode,
                  nepoch,
                  evaluate_loss_after,
                  stop_threshold,
                  learning_rate,
                  lambda1,
                  lambda2,
                  p_drop,
                  mu=None,
                  mu2=None,
                  model_file=None):

        # ---------- HYPERPARAMETERS ----------

        # learning rate
        self.learning_rate.set_value(learning_rate)

        # L1 and L2 regularization parameters
        self.reg_lambda1.set_value(lambda1)
        self.reg_lambda2.set_value(lambda2)

        # gradient update hyperparameters
        if (mu == None) and (grad_mode == "momentum" or grad_mode == "nesterov" or grad_mode == "rmsprop" or grad_mode == "adam"):
            raise ValueError("Selected grad mode (" + grad_mode + ") requires parameter mu")
        if (mu2 == None) and (grad_mode == "adam"):
            raise ValueError("Selected grad mode (" + grad_mode + ") requires parameter mu2")
        self.mu.set_value(mu)
        self.mu2.set_value(mu2)

        # dropout probability
        self.p_drop.set_value(p_drop)

        # current iteration (for
        self.iteration.set_value(1)

        best_loss = np.inf # Best value of the loss function so far
        losses = []  # Keep track of the losses
        for epoch in range(nepoch):

            for train_batch in training_data.iterate():
                xtrain, ytrain = train_batch[0], train_batch[1]
                if grad_mode == "vanilla":
                    self.sgd_step_vanilla(xtrain, ytrain)
                elif grad_mode == "momentum":
                    self.sgd_step_momentum(xtrain, ytrain)
                elif grad_mode == "nesterov":
                    self.sgd_step_nesterov(xtrain, ytrain)
                elif grad_mode == "adagrad":
                    self.sgd_step_adagrad(xtrain, ytrain)
                elif grad_mode == "rmsprop":
                    self.sgd_step_rmsprop(xtrain, ytrain)
                elif grad_mode == "adam":
                    self.sgd_step_adam(xtrain, ytrain)
                else:
                    print 'sgd_train: Invalid grad_mode ID'
                    return

            # Evaluate the loss on validation set
            if epoch % evaluate_loss_after == 0:
                loss = 0
                for valid_batch in valid_data.iterate():
                    xvalid, yvalid = valid_batch[0], valid_batch[1]
                    loss += self.calculate_loss(xvalid, yvalid)
                loss /= len(valid_data.items)
                losses.append(loss)
                _time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
                print "%s: Loss at epoch=%d: %f" % (_time, epoch, loss)

                # Save model parameters
                if loss < best_loss:
                    best_loss = loss
                    self.save_model_parameters_theano(model_file)

                # Adjust the learning rate
                if grad_mode == "vanilla" or grad_mode == "momentum" or grad_mode == "nesterov":
                    self.annealing(losses, mode=annealing_mode)

                for cb in self.events_cbs:
                    cb(self, "ev_iteration")

                #if (len(losses) > stop_threshold and min(losses[-stop_threshold:]) == losses[-stop_threshold]) or self.learning_rate.get_value() <= 1e-10:
                #if (len(losses) > stop_threshold and min(losses[-stop_threshold:]) == losses[-stop_threshold]):
                #    print "Stop threshold reached"
                #    break
        print "Reverting to best model."
        self.load_model_parameters_theano(model_file)
        for cb in self.events_cbs:
            cb(self, "ev_end", {"grad_mode": grad_mode,
                                "annealing_mode": annealing_mode,
                                "nepoch": nepoch,
                                "epoch": epoch,
                                "learning_rate": learning_rate})
        return losses

    # Mean Squared Error
    def mse(self, output, target):
        series_length = T.max(output.shape)
        return T.sum((output - target) ** 2) / series_length


    # Normalized Rooted Mean Squared Error
    def nrmse(self, output, target):
        target_var = T.var(target)
        return T.sqrt(self.mse(output, target) / target_var)

    # Add a regularization term to the norm function. The possible options can be selected by setting the lambda values:
    # 'reg_lambda1' = 0 and 'reg_lambda2' = 0 --> no regularization (default)
    # 'reg_lambda1' != 0 and 'reg_lambda2' = 0 --> LASSO regularization
    # 'reg_lambda1' = 0 and 'reg_lambda2' != 0 --> L2 regularization
    # 'reg_lambda1' != 0 and 'reg_lambda2' != 0 --> elastic net penalty
    def regularize(self, params, reg_lambda1=0, reg_lambda2=0):
        loss_reg = 0
        for p in params:
            pdim = p.flatten().shape[0]
            loss_reg += (reg_lambda1 * T.sum(T.abs_(p)) + reg_lambda2 / 2 * T.sum(T.pow(p, 2))) / T.sqrt(pdim)
        return loss_reg

    # Save model parameters to 'outfile'
    def save_model_parameters_theano(self, file_name = None, verbose = False):
        if file_name:
            params_numpy = {k: p.get_value() for k, p in self.params.iteritems()}
            saved_data = dict(s_hidden_dim=self.hidden_dim, s_params=params_numpy, s_net_type=self.net_type)
            with open(file_name, 'wb') as file_w:
                pickle.dump(saved_data, file_w, protocol=pickle.HIGHEST_PROTOCOL)
            if verbose:
                print "Saved model parameters to %s." % file_name
        else:
            self.params_numpy = {k: p.get_value() for k, p in self.params.iteritems()}
            print "Saved model parameters to memory."


    # Load model parameters from 'path'
    def load_model_parameters_theano(self, file_name = None, verbose = False):
        if file_name:
            with open(file_name, 'rb') as file_r:
                saved_data = pickle.load(file_r)
            self.hidden_dim = saved_data["s_hidden_dim"]
            self.net_type = saved_data["s_net_type"]
            for key in saved_data["s_params"]:
                self.params[key].set_value(saved_data["s_params"][key])
            if verbose:
                print "Loaded model parameters from %s. Network type: %s with %d neurons" % (file_name, saved_data["s_net_type"], saved_data["s_hidden_dim"])
        else:
            for key in self.params_numpy:
                self.params[key].set_value(self.params_numpy[key])
            print "Loaded model parameters from memory."


    # Set of methods for annealing the learning rate.
    def annealing(self, losses, mode, a0_ann=0.1, k_ann=1.1):

        # At each iteration halve the learning rate
        if mode == 'step1':
            self.learning_rate.set_value(self.learning_rate.get_value() * 0.5)
            print "Learning rate %f" % self.learning_rate.get_value()

        # If the learning rate has not improved enough since the last check, halve it
        elif mode == 'step2':
            if len(losses) > 1 and (losses[-2] - losses[-1]) <= 0.00001:
                self.learning_rate.set_value(self.learning_rate.get_value() * 0.5)
                print "Learning rate %f" % self.learning_rate.get_value()

        # The learning rate decays exponentially at each iteration
        elif mode == 'exp':
            self.learning_rate.set_value(a0_ann*np.exp(-k_ann*len(losses)))
            print "Learning rate %f" % self.learning_rate.get_value()

        # The learning rate decays proportionally with the number of iterations
        elif mode == 'inv':
            self.learning_rate.set_value(a0_ann/(1 + k_ann*len(losses)))
            print "Learning rate %f" % self.learning_rate.get_value()

        # No annealing
        elif mode is None:
            pass

        else:
            raise ValueError("annealing: Invalid annealing method (" + mode + "). Allowed values are step1, step2, exp, inv.")
