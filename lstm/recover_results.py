__author__ = 'enrico'
# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import time
from lstmutils import *
from lstmef import LSTMTheano
#from scipy import stats
import matplotlib.pyplot as plt
#from hf import SequenceDataset
import sys
import os

# Define parameters
NET_TYPE = sys.argv[1]
TS_FILENAME = sys.argv[2]
if len(sys.argv) == 4:
    STARTING_ITERATION = int(sys.argv[3])
else:
    STARTING_ITERATION = 0
LEARNING_RATE = float(0.01)  # Initial value of the learning rate
NEPOCH = 5000  # Number of epochs for the SGD optimization
EVAL_LOSS_AFT = 100  # Number of epochs after which the loss must be evaluated (defines the update frequency for learning rate)
STOP_THRESH = 5 # Number of epochs*EVAL_LOSS_AFT after which the algorithm stops if no better loss has been found
OPTIMIZATION = 'SGD'  # Optimization type: SGD or HF
GRAD_MODE = 'vanilla'
ANNEAL_MODE = 'step2'
VS_PERC = 0.15  # Percentage of dataset used as validation
TS_PERC = 0.15  # Percentage of dataset used as test
BPTT_TRUNC = 10  # Length of bptt
REG_LAMBDA1 = 0.0  # L1 regularization parameter
REG_LAMBDA2 = 0.01  # L2 regularization parameter
#NN_HIDDEN = 15  # number of hidden units
NN_INPUT = 1  # number of input units
NN_OUTPUT = 1  # number of output units
MU = float(0.9)  # momentum, nesterov, rmsprop hyperparameter
MU2 = float(0.999)  # adam hyperparameter
P_DROP = 1.0  # dropout probability (probability of keeping a neuron active)


DATASET_FOLDER = "../Dataset/"
RESULTS_FOLDER = "../Results/" + NET_TYPE + "/" + TS_FILENAME + "/"
MODELS_FOLDER = RESULTS_FOLDER + "Models/"
if not os.path.exists(RESULTS_FOLDER):
    os.makedirs(RESULTS_FOLDER)
if not os.path.exists(MODELS_FOLDER):
    os.makedirs(MODELS_FOLDER)
MODEL_FILE = RESULTS_FOLDER + 'temp_model.pickle'  # Name of the file for saving network parameters


# Load dataset
ts_dataset = np.loadtxt(fname=DATASET_FOLDER + TS_FILENAME, delimiter='\t')
print 'Dataset read: {0:d} rows and {1:d} column'.format(ts_dataset.shape[0], ts_dataset.shape[1])
ts_dataset = data_normalization(ts_dataset, 'zscore')
dataset_length = ts_dataset.shape[0]
vs_length = int(dataset_length * VS_PERC)
ts_length = int(dataset_length * TS_PERC)
obsData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 0]).T
futureData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 1]).T
obsData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 0]).T
futureData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 1]).T
obsData_ts = np.atleast_2d(ts_dataset[-ts_length:, 0]).T
futureData_ts = np.atleast_2d(ts_dataset[-ts_length:, 1]).T
train_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)
cg_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)  # used to compute correlated gradient in HF optimization (must be similar to training data)
valid_dataset = SequenceDataset([[obsData_vs], [futureData_vs]], batch_size=vs_length)


# Generate a new model or load an existing one

nn_hidden_list = [10,20,30]
#
# log_params = [['task', lambda model: TS_FILENAME],
#               ['nn_hidden', lambda model: model.hidden_dim],
#               ['optimization', lambda model: OPTIMIZATION],
#               ['grad_mode'],
#               ['learning_rate'],
#               ['lambda1', lambda model: model.reg_lambda1.get_value()],
#               ['lambda2', lambda model: model.reg_lambda2.get_value()],
#               ['p_drop', lambda model: model.p_drop.get_value()],
#               ['epoch'],
#               ['err', lambda model: np_nrmse(model.predict(obsData_ts), futureData_ts)]]

#logger = RNNLogger(RESULTS_FOLDER + "results.dat", log_params, newfile = True, models_folder = MODELS_FOLDER)

files = os.listdir(MODELS_FOLDER)
files.sort()

with open(RESULTS_FOLDER + "results_recover.dat", "a") as recfile:
    for nn_hidden_ind in range(len(nn_hidden_list)):
        nn_hidden = nn_hidden_list[nn_hidden_ind]
        print "Recover for nn_hidden = ", nn_hidden, "..."
        model = LSTMTheano(NN_INPUT, NN_OUTPUT, nn_hidden, NET_TYPE, bptt_truncate=BPTT_TRUNC)
        for f_i in range(nn_hidden_ind * 120, (nn_hidden_ind+1) * 120):
            print files[f_i]
            model.load_model_parameters_theano(MODELS_FOLDER + files[f_i], True)
            err = np_nrmse(model.predict(obsData_ts), futureData_ts)
            recfile.write(files[f_i] + "\t" + TS_FILENAME + str(err) + "\n")
        del model