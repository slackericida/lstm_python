__author__ = 'enrico'
# Authors:
# Enrico Maiorino - enrico.maiorino@gmail.com
# Filippo Maria Bianchi - filippombianchi@gmail.com

import time
from lstmutils import *
from lstmef import LSTMTheano
#from scipy import stats
import matplotlib.pyplot as plt
#from hf import SequenceDataset

# Define parameters
LEARNING_RATE = float(0.01)  # Initial value of the learning rate
NEPOCH = 5000  # Number of epochs for the SGD optimization
EVAL_LOSS_AFT = 100  # Number of epochs after which the loss must be evaluated (defines the update frequency for learning rate)
STOP_THRESH = 5 # Number of epochs*EVAL_LOSS_AFT after which the algorithm stops if no better loss has been found
NET_TYPE = 'GRU'
OPTIMIZATION = 'SGD'  # Optimization type: SGD or HF
GRAD_MODE = 'vanilla'
ANNEAL_MODE = 'step2'
VS_PERC = 0.15  # Percentage of dataset used as validation
TS_PERC = 0.15  # Percentage of dataset used as test
BPTT_TRUNC = 10  # Length of bptt
REG_LAMBDA1 = 0.0  # L1 regularization parameter
REG_LAMBDA2 = 0.01  # L2 regularization parameter
#NN_HIDDEN = 15  # number of hidden units
NN_INPUT = 1  # number of input units
NN_OUTPUT = 1  # number of output units
MU = float(0.9)  # momentum, nesterov, rmsprop hyperparameter
MU2 = float(0.999)  # adam hyperparameter
P_DROP = 1.0  # dropout probability (probability of keeping a neuron active)

TS_FILENAME = "MG"
DATASET_FOLDER = "../Dataset/"
RESULTS_FOLDER = "../Results/" + NET_TYPE + "/" + TS_FILENAME + "/"
MODELS_FOLDER = RESULTS_FOLDER + "Models/"
if not os.path.exists(RESULTS_FOLDER):
    os.makedirs(RESULTS_FOLDER)
if not os.path.exists(MODELS_FOLDER):
    os.makedirs(MODELS_FOLDER)
MODEL_FILE = RESULTS_FOLDER + 'temp_model.pickle'  # Name of the file for saving network parameters


# Load dataset
ts_dataset = np.loadtxt(fname=DATASET_FOLDER + TS_FILENAME, delimiter='\t')
print 'Dataset read: {0:d} rows and {1:d} column'.format(ts_dataset.shape[0], ts_dataset.shape[1])
ts_dataset = data_normalization(ts_dataset, 'zscore')
dataset_length = ts_dataset.shape[0]
vs_length = int(dataset_length * VS_PERC)
ts_length = int(dataset_length * TS_PERC)
obsData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 0]).T
futureData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 1]).T
obsData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 0]).T
futureData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 1]).T
obsData_ts = np.atleast_2d(ts_dataset[-ts_length:, 0]).T
futureData_ts = np.atleast_2d(ts_dataset[-ts_length:, 1]).T
train_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)
cg_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)  # used to compute correlated gradient in HF optimization (must be similar to training data)
valid_dataset = SequenceDataset([[obsData_vs], [futureData_vs]], batch_size=vs_length)


# Generate a new model or load an existing one

nn_hidden_list = [10,20,30]

log_params = [['task', lambda model: TS_FILENAME],
              ['nn_hidden', lambda model: model.hidden_dim],
              ['optimization', lambda model: OPTIMIZATION],
              ['grad_mode'],
              ['learning_rate', lambda model: model.learning_rate.get_value()],
              ['lambda1', lambda model: model.reg_lambda1.get_value()],
              ['lambda2', lambda model: model.reg_lambda2.get_value()],
              ['p_drop', lambda model: model.p_drop.get_value()],
              ['epoch'],
              ['err', lambda model: np_nrmse(model.predict(obsData_ts), futureData_ts)]]

logger = RNNLogger(RESULTS_FOLDER + "results.dat", log_params, newfile = True, models_folder = MODELS_FOLDER)

for nn_hidden in nn_hidden_list:
    model = LSTMTheano(NN_INPUT, NN_OUTPUT, nn_hidden, NET_TYPE, bptt_truncate=BPTT_TRUNC)

    model.events_cbs.append(logger.hook)

    scheduler = Scheduler(model, model.sgd_train, save_filename=RESULTS_FOLDER + "schedule_nnh" + str(nn_hidden) + ".pickle")

    print
    print "-------------------------------"
    print "Schedule learning_rate x p_drop (lambda2 = 0.01)"
    print "-------------------------------"
    print
    learning_rate_list = 2. ** ((-1)*np.arange(5,14,2))
    p_drop_list = [1.,0.97,0.95,0.93,0.9,0.8]
    scheduler.multi_schedule([["learning_rate"],["p_drop"]], train_dataset, valid_dataset, grad_mode=GRAD_MODE, annealing_mode=ANNEAL_MODE,
                             nepoch=NEPOCH, evaluate_loss_after=EVAL_LOSS_AFT, stop_threshold=STOP_THRESH, learning_rate=learning_rate_list,
                             lambda1=REG_LAMBDA1, lambda2=REG_LAMBDA2, p_drop=p_drop_list, mu=MU, mu2=MU2, model_file=MODEL_FILE)

    print
    print "-------------------------------"
    print "Schedule learning rate x (lambda1 . lambda2)"
    print "-------------------------------"
    print
    learning_rate_list = 2. ** ((-1)*np.arange(5,14,2))
    lambda1_list = [0, 0.01, 0.05, 0.1, 0.3, 0.5]
    lambda2_list = [0, 0.01, 0.05, 0.1, 0.3, 0.5]
    scheduler.multi_schedule([["learning_rate"],["lambda1","lambda2"]], train_dataset, valid_dataset, grad_mode=GRAD_MODE, annealing_mode=ANNEAL_MODE,
                             nepoch=NEPOCH, evaluate_loss_after=EVAL_LOSS_AFT, stop_threshold=STOP_THRESH, learning_rate=learning_rate_list,
                             lambda1=lambda1_list, lambda2=lambda2_list, p_drop=P_DROP, mu=MU, mu2=MU2, model_file=MODEL_FILE)

    print
    print "-------------------------------"
    print "Schedule learning rate x lambda1 (lambda2 = 0)"
    print "-------------------------------"
    print
    learning_rate_list = 2. ** ((-1)*np.arange(5,14,2))
    lambda1_list = [0, 0.01, 0.05, 0.1, 0.3, 0.5]
    lambda2 = 0.
    scheduler.multi_schedule([["learning_rate"],["lambda1"]], train_dataset, valid_dataset, grad_mode=GRAD_MODE, annealing_mode=ANNEAL_MODE,
                             nepoch=NEPOCH, evaluate_loss_after=EVAL_LOSS_AFT, stop_threshold=STOP_THRESH, learning_rate=learning_rate_list,
                             lambda1=lambda1_list, lambda2=lambda2, p_drop=P_DROP, mu=MU, mu2=MU2, model_file=MODEL_FILE)

    print
    print "-------------------------------"
    print "Schedule learning rate x lambda2 (lambda1 = 0)"
    print "-------------------------------"
    print
    learning_rate_list = 2. ** ((-1)*np.arange(5,14,2))
    lambda1 = 0.
    lambda2_list = [0, 0.01, 0.05, 0.1, 0.3, 0.5]
    scheduler.multi_schedule([["learning_rate"],["lambda2"]], train_dataset, valid_dataset, grad_mode=GRAD_MODE, annealing_mode=ANNEAL_MODE,
                             nepoch=NEPOCH, evaluate_loss_after=EVAL_LOSS_AFT, stop_threshold=STOP_THRESH, learning_rate=learning_rate_list,
                             lambda1=lambda1, lambda2=lambda2_list, p_drop=P_DROP, mu=MU, mu2=MU2, model_file=MODEL_FILE)


    losses = scheduler.execute_all()