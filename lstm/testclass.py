__author__ = 'enrico'
import itertools
begindict = {"var1":[1,2,3], "var2":['a','b','c'], "var3":['x','y'], "var4":[10,20], "ciao":[3,4,5]}
#arglist = [[[1,2,3],['a','b','c']],[['x','y']]]
variable_indexes = [["var1","var2", 0],["var3", "var4"]]

#print [var_ind for i in arglist for var_ind in i]

#print [begindict[a] for a in arglist[0]]
#newlist = itertools.product(*[zip(*[[[a,e] for e in begindict[a]] for a in arg]) for arg in arglist])
#print list(newlist)
# for elem in newlist:
#     print [element for tupl in elem for element in tupl]


def multi_schedule(self, variable_index, *args, **kwargs):
    if isinstance(variable_index, int):
        for i in range(len(args[variable_index])):
            newargs = list(args)
            newargs[variable_index] = args[variable_index][i]
            self.schedule(*newargs, **kwargs)
    elif isinstance(variable_index, basestring):
        for i in range(len(kwargs[variable_index])):
            newkwargs = kwargs.copy()
            newkwargs[variable_index] = kwargs[variable_index][i]
            self.schedule(*args, **newkwargs)
    elif isinstance(variable_index, list):
        vars = [var_ind for i in variable_index for var_ind in i]
        vars_dict = {}
        for v in vars:
            if isinstance(v, int):
                vars_dict[v] = args[v]
            else:
                vars_dict[v] = kwargs[v]
        # next instruction creates the list of arguments lists where each argument is listed as [arg_name, value]
        arglists = itertools.product(*[zip(*[[[a,e] for e in vars_dict[a]] for a in arg]) for arg in variable_index])
        for arglist in arglists:
            f_arglist = [elem for temp in arglist for elem in temp] # flatten arglist array
            newargs = list(args)
            newkwargs = kwargs.copy()
            for arg in f_arglist:
                if isinstance(arg[0], int):
                    newargs[arg[0]] = arg[1]
                elif isinstance(arg[0], basestring):
                    newkwargs[arg[0]] = arg[1]
            self.schedule(*newargs, **newkwargs)
    else:
        raise ValueError("Invalid variable_index provided.")


multi_schedule(variable_indexes, [100,200,300], [4000,5000], **begindict)