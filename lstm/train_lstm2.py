import time
from lstmutils import *
from lstmef import LSTMTheano
#from scipy import stats
import matplotlib.pyplot as plt
#from hf import SequenceDataset
import sys
import os
import time

# NEPOCH, RESULTS_FOLDER, BPTT_TRUNC, fast_run

# Define parameters
NET_TYPE = sys.argv[1]
TS_FILENAME = sys.argv[2]
NN_HIDDEN = int(sys.argv[3])
LEARNING_RATE = float(sys.argv[4])
GRAD_MODE = sys.argv[5]
REG_LAMBDA1 = float(sys.argv[6])  # L1 regularization parameter
REG_LAMBDA2 = float(sys.argv[7])  # L2 regularization parameter
P_DROP = float(sys.argv[8])  # dropout probability (probability of keeping a neuron active)
NEPOCH = 5001  # Number of epochs for the SGD optimization
EVAL_LOSS_AFT = 100  # Number of epochs after which the loss must be evaluated (defines the update frequency for learning rate)
STOP_THRESH = 5 # Number of epochs*EVAL_LOSS_AFT after which the algorithm stops if no better loss has been found
OPTIMIZATION = 'SGD'  # Optimization type: SGD or HF
ANNEAL_MODE = 'step2'
VS_PERC = 0.15  # Percentage of dataset used as validationi
TS_PERC = 0.15  # Percentage of dataset used as test
BPTT_TRUNC = 10  # Length of bptt
NN_INPUT = 1  # number of input units
NN_OUTPUT = 1  # number of output units
MU = float(0.9)  # momentum, nesterov, rmsprop hyperparameter
MU2 = float(0.999)  # adam hyperparameter

DATE_ID = time.strftime("%Y-%m-%d-%H-%M-%S")
TEST_ID = DATE_ID + "-{}-{}-H{}-L{}-{}-R{}-R{}-P{}".format(NET_TYPE, TS_FILENAME, NN_HIDDEN, LEARNING_RATE, GRAD_MODE, REG_LAMBDA1, REG_LAMBDA2, P_DROP)

DATASET_FOLDER = "../Dataset/"
RESULTS_FOLDER = "../Results/" + NET_TYPE + "/" + TS_FILENAME + "/"
MODELS_FOLDER = RESULTS_FOLDER + "Models/"
if not os.path.exists(RESULTS_FOLDER):
    os.makedirs(RESULTS_FOLDER)
if not os.path.exists(MODELS_FOLDER):
    os.makedirs(MODELS_FOLDER)
#TEMP_MODEL_FILE = RESULTS_FOLDER + 'temp_' + TEST_ID + '.pickle'  # Name of the temporary file for saving network parameters
TEMP_MODEL_FILE = None

# Load dataset
ts_dataset = np.loadtxt(fname=DATASET_FOLDER + TS_FILENAME, delimiter='\t')
print 'Dataset read: {0:d} rows and {1:d} column'.format(ts_dataset.shape[0], ts_dataset.shape[1])
ts_dataset = data_normalization(ts_dataset, 'zscore')
dataset_length = ts_dataset.shape[0]
vs_length = int(dataset_length * VS_PERC)
ts_length = int(dataset_length * TS_PERC)
obsData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 0]).T
futureData_tr = np.atleast_2d(ts_dataset[:-(vs_length+ts_length), 1]).T
obsData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 0]).T
futureData_vs = np.atleast_2d(ts_dataset[-(vs_length+ts_length):-ts_length, 1]).T
obsData_ts = np.atleast_2d(ts_dataset[-ts_length:, 0]).T
futureData_ts = np.atleast_2d(ts_dataset[-ts_length:, 1]).T
train_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)
cg_dataset = SequenceDataset([[obsData_tr], [futureData_tr]], batch_size=150)  # used to compute correlated gradient in HF optimization (must be similar to training data)
valid_dataset = SequenceDataset([[obsData_vs], [futureData_vs]], batch_size=vs_length)

model = LSTMTheano(NN_INPUT, NN_OUTPUT, NN_HIDDEN, NET_TYPE, bptt_truncate=BPTT_TRUNC)

model.sgd_train(train_dataset, valid_dataset, grad_mode=GRAD_MODE, annealing_mode=ANNEAL_MODE,
                                 nepoch=NEPOCH, evaluate_loss_after=EVAL_LOSS_AFT, stop_threshold=STOP_THRESH, learning_rate=LEARNING_RATE,
                                 lambda1=REG_LAMBDA1, lambda2=REG_LAMBDA2, p_drop=P_DROP, mu=MU, mu2=MU2, model_file=TEMP_MODEL_FILE)

model.save_model_parameters_theano(MODELS_FOLDER + TEST_ID + ".pickle")
error = np_nrmse(model.predict(obsData_ts), futureData_ts)

log_params = [['TestID', TEST_ID],
              ['net_type', NET_TYPE],
              ['task', TS_FILENAME],
              ['nn_hidden', NN_HIDDEN],
              ['optimization', OPTIMIZATION],
              ['grad_mode', GRAD_MODE],
              ['learning_rate', LEARNING_RATE],
              ['lambda1', REG_LAMBDA1],
              ['lambda2', REG_LAMBDA2],
              ['p_drop', P_DROP],
              ['err', error]]

with open(RESULTS_FOLDER + TEST_ID + ".dat", "w") as file:
    file.write("\t".join([p[0] for p in log_params]) + "\n") # creates the variable titles in the logfile
    file.write("\t".join([str(p[1]) for p in log_params]) + "\n")

if TEMP_MODEL_FILE is not None: os.remove(TEMP_MODEL_FILE)